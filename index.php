<?php

$db = __DIR__.'/urls.db';
$db = new PDO("sqlite:$db");

if ($id = getenv('QUERY_STRING')) {
	$id = base_convert($id, 36, 10);
	$url = $db->query("select url from urls where id = $id")->fetchColumn();
	header("location: $url");
	die;
}

if ($url = @$_POST['url']) {
	$url = $db->quote($url);
	$db->query("insert into urls (url) values ($url)");
	$id = $db->lastInsertId();
	$id = base_convert($id, 10, 36);
	echo $id;
	die;
}

?>

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tiny-tiny URL</title>
</head>
<body>
	<h1>Tiny-tiny URL</h1>
	<form id="frm" action="" method="post">
		<input id="url" type="text" name="url">
		<input id="submit" type="submit" value="go">
	</form>
	<script>
		document.getElementById('submit').addEventListener('click', function(e) {
			e.preventDefault();
			var x = new XMLHttpRequest();
			x.open('post', document.getElementById('frm').action, true);
			x.setRequestHeader('Content-type','application/x-www-form-urlencoded');
			x.onreadystatechange = function () {
				if (x.readyState != 4) return;
				var text = x.status == 200 ? x.responseText : 'error';
				alert(location.href+'?'+text);
			}
			var url = document.getElementById('url').value;
			x.send('url='+encodeURIComponent(url));
		}, false);
	</script>
</body>
</html>

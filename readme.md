# Shurl

URL shortener (can be used for magnet links)

## Install

	$ git clone https://bitbucket.org/polonskiy/shurl.git
	$ sqlite3 urls.db < install.sql
	$ chmod a=rw urls.db
